#-------------------------------------------------
#
# Project created by QtCreator 2018-07-20T09:12:54
#
#-------------------------------------------------

QT      += core gui widgets

TARGET   = corepaint
TEMPLATE = app

VERSION  = 3.0.0

# Library section
unix:!macx: LIBS += -lcprime

# thread - Enable threading support
# silent - Do not print the compilation syntax
CONFIG  += thread silent

# Disable QDebug on Release build
CONFIG(release, debug|release):DEFINES += QT_NO_DEBUG_OUTPUT

# Warn about deprecated features
DEFINES += QT_DEPRECATED_WARNINGS
# Disable all deprecated features before Qt 5.15
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x051500

# Build section
BUILD_PREFIX = $$(CA_BUILD_DIR)

isEmpty( BUILD_PREFIX ) {
        BUILD_PREFIX = ./build
}

MOC_DIR       = $$BUILD_PREFIX/moc-qt5
OBJECTS_DIR   = $$BUILD_PREFIX/obj-qt5
RCC_DIR	      = $$BUILD_PREFIX/qrc-qt5
UI_DIR        = $$BUILD_PREFIX/uic-qt5

# C++17 Support for Qt5
QMAKE_CXXFLAGS += -std=c++17

unix {
        isEmpty(PREFIX) {
                PREFIX = /usr
        }
        BINDIR = $$PREFIX/bin

        target.path = $$BINDIR

        desktop.path = $$PREFIX/share/applications/
        desktop.files = "corepaint.desktop"

        icons.path = /usr/share/icons/hicolor/scalable/apps/
        icons.files = icons/corepaint.svg

        INSTALLS += target icons desktop
}


FORMS += \
    corepaint.ui

HEADERS += \
    dialogs/resizedialog.h \
    dialogs/textdialog.h \
    instruments/abstractinstrument.h \
    instruments/abstractselection.h \
    instruments/colorpickerinstrument.h \
    instruments/curvelineinstrument.h \
    instruments/ellipseinstrument.h \
    instruments/eraserinstrument.h \
    instruments/fillinstrument.h \
    instruments/lineinstrument.h \
    instruments/pencilinstrument.h \
    instruments/rectangleinstrument.h \
    instruments/selectioninstrument.h \
    instruments/sprayinstrument.h \
    instruments/textinstrument.h \
    widgets/colorchooser.h \
    additionaltools.h \
    corepaint.h \
    datasingleton.h \
    easypaintenums.h \
    imagearea.h \
    undocommand.h

SOURCES += \
    dialogs/resizedialog.cpp \
    dialogs/textdialog.cpp \
    instruments/abstractinstrument.cpp \
    instruments/abstractselection.cpp \
    instruments/colorpickerinstrument.cpp \
    instruments/curvelineinstrument.cpp \
    instruments/ellipseinstrument.cpp \
    instruments/eraserinstrument.cpp \
    instruments/fillinstrument.cpp \
    instruments/lineinstrument.cpp \
    instruments/pencilinstrument.cpp \
    instruments/rectangleinstrument.cpp \
    instruments/selectioninstrument.cpp \
    instruments/sprayinstrument.cpp \
    instruments/textinstrument.cpp \
    widgets/colorchooser.cpp \
    additionaltools.cpp \
    corepaint.cpp \
    datasingleton.cpp \
    imagearea.cpp \
    main.cpp \
    undocommand.cpp

RESOURCES += \
    icons.qrc
