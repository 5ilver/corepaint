# CorePaint
A paint app for C Suite.

<img src="corepaint.png" width="500">

### Download
You can download latest release.
* [Source](https://gitlab.com/cubocore/corepaint/tags)
* [ArchPackages](https://gitlab.com/cubocore/wiki/tree/master/ArchPackages)

### Dependencies:
* qt5-base
* [libcprime](https://gitlab.com/cubocore/libcprime) 

### Information
Please see the Wiki page for this info.[Wiki page](https://gitlab.com/cubocore/wiki) .
* Changes in latest release
* Build from the source
* Tested In
* Known Bugs
* Help Us

### Feedback
* We need your feedback to improve the CoreApps.Send us your feedback through GitLab [issues](https://gitlab.com/groups/cubocore/-/issues).
