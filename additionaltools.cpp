/*
    *
    * This file is a part of CorePaint.
    * A paint app for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QPixmap>
#include <QImage>
#include <QPainter>
#include <QLabel>
#include <QTransform>
#include <QSize>
#include <QClipboard>
#include <QApplication>

#include "additionaltools.h"
#include "imagearea.h"
#include "dialogs/resizedialog.h"

AdditionalTools::AdditionalTools(ImageArea *pImageArea, QObject *parent) : QObject(parent)
{
    mPImageArea = pImageArea;
    mZoomedFactor = 1;
}

AdditionalTools::~AdditionalTools()
{

}

void AdditionalTools::resizeCanvas(int width, int height, bool flag)
{
    if (flag) {
        ResizeDialog resizeDialog(QSize(width, height), qobject_cast<QWidget *>(this->parent()));

        if (resizeDialog.exec() == QDialog::Accepted) {
            QSize newSize = resizeDialog.getNewSize();
            width = newSize.width();
            height = newSize.height();
        } else {
            return;
        }
    }

    if (width < 1 || height < 1) {
        return;
    }

    QImage *tempImage = new QImage(width, height, QImage::Format_ARGB32_Premultiplied);
    QPainter painter(tempImage);
    painter.setPen(Qt::NoPen);
    painter.setBrush(QBrush(Qt::white));
    painter.drawRect(QRect(0, 0, width, height));
    painter.drawImage(0, 0, *mPImageArea->getImage());
    painter.end();

    mPImageArea->setImage(*tempImage);

    mPImageArea->resize(mPImageArea->getImage()->rect().right() + 15,
                        mPImageArea->getImage()->rect().bottom() + 15);
    mPImageArea->setEdited(true);
    mPImageArea->clearSelection();
}

void AdditionalTools::resizeImage()
{
    ResizeDialog resizeDialog(mPImageArea->getImage()->size(), qobject_cast<QWidget *>(this->parent()));

    if (resizeDialog.exec() == QDialog::Accepted) {
        mPImageArea->setImage(mPImageArea->getImage()->scaled(resizeDialog.getNewSize()));
        mPImageArea->resize(mPImageArea->getImage()->rect().right() + 15,
                            mPImageArea->getImage()->rect().bottom() + 15);
        mPImageArea->setEdited(true);
        mPImageArea->clearSelection();
    }
}

void AdditionalTools::rotateImage(bool flag)
{
    QTransform transform;

    if (flag) {
        transform.rotate(90);
    } else {
        transform.rotate(-90);
    }

    mPImageArea->setImage(mPImageArea->getImage()->transformed(transform));
    mPImageArea->resize(mPImageArea->getImage()->rect().right() + 15,
                        mPImageArea->getImage()->rect().bottom() + 15);
    mPImageArea->update();
    mPImageArea->setEdited(true);
    mPImageArea->clearSelection();
}

bool AdditionalTools::zoomImage(qreal factor)
{
    mZoomedFactor *= factor;

    if (mZoomedFactor < 0.01) {
        mZoomedFactor = 0.01;
        return false;
    }

    else if (mZoomedFactor > 4) {
        mZoomedFactor = 4;
        return false;
    }

    else {

		/* Image size to which we should scale */
        QSize size = mPImageArea->/*getImageCopy().*/getImage()->size() * /*mZoomedFactor*/factor;
        qWarning() << mPImageArea->/*getImageCopy().*/getImage()->size() << size;

        mPImageArea->setImage(mPImageArea->/*getImageCopy().*/getImage()->scaled(size, Qt::KeepAspectRatio, Qt::SmoothTransformation));
        mPImageArea->resize(static_cast<int>((mPImageArea->rect().width())*factor), static_cast<int>((mPImageArea->rect().height())*factor));

        emit sendNewImageSize(mPImageArea->size());

        mPImageArea->setEdited(true);
        mPImageArea->clearSelection();

        return true;
    }
}
