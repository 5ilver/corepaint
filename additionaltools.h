/*
	*
	* This file is a part of CorePaint.
	* A paint app for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#ifndef ADDITIONALTOOLS_H
#define ADDITIONALTOOLS_H

#include <QObject>


QT_BEGIN_NAMESPACE
class ImageArea;
class QSize;
QT_END_NAMESPACE


class AdditionalTools : public QObject
{
    Q_OBJECT

public:
    explicit AdditionalTools(ImageArea *pImageArea, QObject *parent);
    ~AdditionalTools();

    void resizeCanvas(int width, int height, bool flag = false);
    void resizeImage();
    void rotateImage(bool flag);
    bool zoomImage(qreal factor);
    
private:
    ImageArea *mPImageArea; /**< A pointer to ImageArea */
    qreal mZoomedFactor; /**< Difference between original and current image */

signals:
    void sendNewImageSize(const QSize&);
    
};

#endif // ADDITIONALTOOLS_H
