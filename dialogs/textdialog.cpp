/*
    *
    * This file is a part of CorePaint.
    * A paint app for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QFont>
#include <QFontDialog>
#include <QDialogButtonBox>
#include <QMessageBox>

#include "textdialog.h"


TextDialog::TextDialog(QString text, ImageArea *imageArea, QWidget *parent) :
	QDialog(parent),
	m_imageArea(imageArea)
{
    initializeGui();

    if (!text.isEmpty()) {
        mTextEdit->setText(text);
        QTextCursor cursor(mTextEdit->textCursor());
        cursor.movePosition(QTextCursor::End, QTextCursor::MoveAnchor);
        mTextEdit->setTextCursor(cursor);
    }

    layout()->setSizeConstraint(QLayout::SetFixedSize);
    setWindowTitle(tr("Text"));
}

void TextDialog::initializeGui()
{
    QPushButton *mFontButton = new QPushButton(tr("Select Font..."));
    QDialogButtonBox *buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok |
            QDialogButtonBox::Cancel);
    connect(mFontButton, SIGNAL(clicked()), this, SLOT(selectFont()));
    connect(buttonBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(buttonBox, SIGNAL(rejected()), this, SLOT(cancel()));
    QHBoxLayout *hBox = new QHBoxLayout();
    hBox->addWidget(mFontButton);
    hBox->addWidget(buttonBox);
    mTextEdit = new QTextEdit();
    mTextEdit->setLineWrapMode(QTextEdit::NoWrap);
    QVBoxLayout *mainLayout = new QVBoxLayout();
    mainLayout->addWidget(mTextEdit);
    mainLayout->addLayout(hBox);
    setLayout(mainLayout);
    connect(mTextEdit, SIGNAL(textChanged()), this, SLOT(textChanged()));
}

void TextDialog::textChanged()
{
	emit textChanged(m_imageArea, mTextEdit->toPlainText());
}

void TextDialog::selectFont()
{
    bool ok;
    QFont font = DataSingleton::Instance()->getTextFont();
    font = QFontDialog::getFont(&ok, font, this);

    if (ok) {
        DataSingleton::Instance()->setTextFont(font);
        textChanged();
        mTextEdit->setFocus();
    }
}

void TextDialog::cancel()
{
	emit canceled(m_imageArea);
    QDialog::reject();
}

void TextDialog::reject()
{
    if (mTextEdit->toPlainText().isEmpty() ||
            QMessageBox::question(this, tr("Question"), tr("Clear text?"),
                                  QMessageBox::Yes | QMessageBox::No,
                                  QMessageBox::No) == QMessageBox::Yes) {
		emit canceled(m_imageArea);
    }

    QDialog::reject();
}
