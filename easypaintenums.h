/*
	*
	* This file is a part of CorePaint.
	* A paint app for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#ifndef EASYPAINTENUMS_H
#define EASYPAINTENUMS_H

typedef enum
{
    NONE_INSTRUMENT = 0,
    CURSOR,
    ERASER,
    PEN,
    LINE,
    COLORPICKER,
    SPRAY,
    FILL,
    RECTANGLE,
    ELLIPSE,
    CURVELINE,
    TEXT,

    // Don't use it. (Used to know count of current instrument)
    INSTRUMENTS_COUNT
} InstrumentsEnum;

#endif // EASYPAINTENUMS_H
