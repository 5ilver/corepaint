/*
    *
    * This file is a part of CorePaint.
    * A paint app for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#ifndef COREPAINT_H
#define COREPAINT_H

#include <QWidget>

#include <cprime/settingsmanage.h>

#include "easypaintenums.h"

QT_BEGIN_NAMESPACE
class QAction;
class QTabWidget;
class PaletteBar;
class ImageArea;
class QLabel;
class QUndoGroup;
class ColorChooser;
class QToolButton;
QT_END_NAMESPACE


namespace Ui {
    class corepaint;
}

class corepaint : public QWidget {
    Q_OBJECT

public:
    explicit corepaint(QWidget *parent = nullptr);
    ~corepaint();

    void sendFiles(const QStringList &paths);
    void initializeNewTab(const bool &isOpen = false, const QString &filePath = "");
    int tabsCount();
    QString saveLocation;

    // Only for save session purpose it is at public
    ImageArea *getImageAreaByIndex(int index);

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void setNewSizeToSizeLabel(const QSize &size);
    void setInstrumentChecked(InstrumentsEnum instrument);
	void setAllInstrumentsUnchecked(QToolButton *actBtn);
    void instumentsAct(bool state);
    void enableCopyCutActions(bool enable);
    void clearImageSelection();
    void restorePreviousInstrument();
    void setInstrument(InstrumentsEnum instrument);
    void on_paintTabs_currentChanged(int index);
    void on_paintTabs_tabCloseRequested(int index);
    void on_newtab_clicked();
    void on_open_clicked();
    void on_save_clicked();
    void on_saveas_clicked();
    void on_resizeimage_clicked();
    void on_resizecanvas_clicked();
    void on_rotateright_clicked();
    void on_rotateleft_clicked();
    void penValueChanged(const int &value);
    void primaryColorChanged(const QColor &color);
    void secondaryColorChanged(const QColor &color);
    void undo();
    void redo();
    void on_menuB_clicked();
    void on_canvasB_clicked();
    void on_selectionB_clicked();
    void on_toolsB_clicked();
    void on_colorB_clicked();
    void on_pinIt_clicked();
    void on_shareIt_clicked();

signals:
    void sendInstrumentChecked(InstrumentsEnum);
    void sendClearImageSelection();

public slots:
    void setPrimaryColorView();
    void setSecondaryColorView();
    void on_cut_clicked();
    void on_copy_clicked();
    void on_past_clicked();
    void on_delet_clicked();

private:
    Ui::corepaint  *ui;
    settingsManage *sm = settingsManage::initialize();
    bool           touch, tablet, mPrevInstrumentSetted;
    int            sideViewSize;
    QSize          sideViewIconSize, toolBarIconSize;
    QUndoGroup    *mUndoStackGroup;
    ColorChooser  *mPColorChooser, *mSColorChooser;
    QString        workFilePath;
	QMap<InstrumentsEnum, QToolButton*> mInstrumentsMap;

    ImageArea *getCurrentImageArea();
    void initializeMainMenu();
    void loadSettings();
    void shotcuts();
    void pageClick(QToolButton *btn, int i);
    bool closeAllTabs();
    bool isSomethingModified();
    void startSetup();
};

#endif // COREPAINT_H
