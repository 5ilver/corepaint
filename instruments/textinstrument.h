/*
	*
	* This file is a part of CorePaint.
	* A paint app for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#ifndef TEXTINSTRUMENT_H
#define TEXTINSTRUMENT_H

#include <QObject>
#include <QPainter>

#include "abstractselection.h"
#include "../imagearea.h"
#include "../datasingleton.h"
#include "../undocommand.h"
#include "../dialogs/textdialog.h"


class TextInstrument : public AbstractSelection
{
    Q_OBJECT
public:
    explicit TextInstrument(QObject *parent = 0);

private:
    void startAdjusting(ImageArea &);
    void startSelection(ImageArea &);
    void startResizing(ImageArea &);
    void startMoving(ImageArea &);
    void select(ImageArea &);
    void resize(ImageArea &imageArea);
    void move(ImageArea &imageArea);
    void completeSelection(ImageArea &imageArea);
    void completeResizing(ImageArea &);
    void completeMoving(ImageArea &);
    void clear();
    void paint(ImageArea &imageArea, bool = false, bool = false);
    void showMenu(ImageArea &imageArea);

    QString mText;
    bool mIsEdited;

signals:
    void sendCloseTextDialog();

private slots:
    void updateText(ImageArea *, QString);
    void cancel(ImageArea *);

};

#endif // TEXTINSTRUMENT_H
