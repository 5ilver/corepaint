/*
	*
	* This file is a part of CorePaint.
	* A paint app for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#ifndef CURVELINEINSTRUMENT_H
#define CURVELINEINSTRUMENT_H

#include "abstractinstrument.h"
#include "../imagearea.h"
#include "../datasingleton.h"

#include <QObject>
#include <QPoint>
#include <QPen>
#include <QPainter>
#include <QImage>


class CurveLineInstrument : public AbstractInstrument
{
    Q_OBJECT
public:
    explicit CurveLineInstrument(QObject *parent = 0);
    
    virtual void mousePressEvent(QMouseEvent *event, ImageArea &imageArea);
    virtual void mouseMoveEvent(QMouseEvent *event, ImageArea &imageArea);
    virtual void mouseReleaseEvent(QMouseEvent *event, ImageArea &imageArea);

protected:
    void paint(ImageArea &imageArea, bool isSecondaryColor = false, bool additionalFlag = false);

private:
    QPoint mFirstControlPoint, mSecondControlPoint;
    unsigned int mPointsCount : 2; /**< Chaneges from 0 to 2, so 2 bits is enough. */

};

#endif // CURVELINEINSTRUMENT_H
