/*
	*
	* This file is a part of CorePaint.
	* A paint app for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#ifndef SELECTIONINSTRUMENT_H
#define SELECTIONINSTRUMENT_H

#include <QPainter>
#include <QApplication>
#include <QClipboard>
#include <QDebug>

#include "math.h"
#include "abstractselection.h"
#include "../imagearea.h"
#include "../undocommand.h"


QT_BEGIN_NAMESPACE
class QUndoStack;
QT_END_NAMESPACE

class SelectionInstrument : public AbstractSelection
{
    Q_OBJECT

public:
    explicit SelectionInstrument(QObject *parent = 0);

    void clearSelectionBackground(ImageArea &imageArea);
    void copyImage(ImageArea &imageArea);
    void pasteImage(ImageArea &imageArea);
    void cutImage(ImageArea &imageArea);

private:
    void startAdjusting(ImageArea &imageArea);
    void startSelection(ImageArea &);
    void startResizing(ImageArea &imageArea);
    void startMoving(ImageArea &imageArea);
    void select(ImageArea &);
    void resize(ImageArea &);
    void move(ImageArea &);
    void completeSelection(ImageArea &imageArea);
    void completeResizing(ImageArea &imageArea);
    void completeMoving(ImageArea &imageArea);
    void clear();
    void paint(ImageArea &imageArea, bool = false, bool = false);
    void showMenu(ImageArea &);

    QImage mSelectedImage,  // Copy of selected image.
           mPasteImage;     //Image to paste

signals:
    void sendEnableCopyCutActions(bool enable);
    void sendEnableSelectionInstrument(bool enable);

};

#endif // SELECTIONINSTRUMENT_H
