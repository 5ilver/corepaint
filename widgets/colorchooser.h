/*
	*
	* This file is a part of CorePaint.
	* A paint app for CuboCore Application Suite
	* Copyright 2019 CuboCore Group
	*

	*
	* This program is free software; you can redistribute it and/or modify
	* it under the terms of the GNU General Public License as published by
	* the Free Software Foundation; either version 3 of the License, or
	* (at your option) any later version.
	*

	*
	* This program is distributed in the hope that it will be useful,
	* but WITHOUT ANY WARRANTY; without even the implied warranty of
	* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	* GNU General Public License for more details.
	*

	*
	* You should have received a copy of the GNU General Public License
	* along with this program; if not, write to the Free Software
	* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
	* MA 02110-1301, USA.
	*
*/


#ifndef COLORCHOOSER_H
#define COLORCHOOSER_H

#include <QLabel>


QT_BEGIN_NAMESPACE
class QColor;
class QPixmap;
class QPainter;
class QMouseEvent;
class QColorDialog;
QT_END_NAMESPACE


class ColorChooser : public QLabel
{
    Q_OBJECT

public:
    explicit ColorChooser(const int &r, const int &g, const int &b,QWidget *parent = nullptr);
    ~ColorChooser();
    
private:
    QColor *mCurrentColor;
    QPixmap *mPixmapColor;
    QPainter *mPainterColor;

public slots:
    void setColor(const QColor &color);

signals:
    void sendColor(const QColor &);

protected:
    void mousePressEvent(QMouseEvent *event);
    
};

#endif // COLORCHOOSER_H
