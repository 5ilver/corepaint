/*
    *
    * This file is a part of CorePaint.
    * A paint app for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#include <QMap>
#include <QMenu>
#include <QApplication>
#include <QAction>
#include <QMenuBar>
#include <QMessageBox>
#include <QScrollArea>
#include <QLabel>
#include <QtEvents>
#include <QPainter>
#include <QInputDialog>
#include <QUndoGroup>
#include <QtCore/QTimer>
#include <QtCore/QMap>
#include <QDateTime>
#include <QSettings>
#include <QShortcut>

#include <cprime/cprime.h>
#include <cprime/themefunc.h>
#include <cprime/pinit.h>
#include <cprime/shareit.h>

#include "imagearea.h"
#include "datasingleton.h"
#include "widgets/colorchooser.h"

#include "corepaint.h"
#include "ui_corepaint.h"


corepaint::corepaint(QWidget *parent): QWidget(parent), ui(new Ui::corepaint),
    mPrevInstrumentSetted(false)
{
    ui->setupUi(this);

    mUndoStackGroup = new QUndoGroup(this);

    qRegisterMetaType<InstrumentsEnum>("InstrumentsEnum");
    DataSingleton::Instance()->setIsInitialized();

    loadSettings();
    startSetup();
    initializeMainMenu();
    shotcuts();
}

corepaint::~corepaint()
{
    delete ui;
}

/**
 * @brief Setup ui elements
 */
void corepaint::startSetup()
{
    // set stylesheet from style.qrc
    ui->sideView->setStyleSheet(CPrime::ThemeFunc::getSideViewStyleSheet());
    ui->sideView->setFixedWidth(sideViewSize);

    ui->appTitle->setAttribute(Qt::WA_TransparentForMouseEvents);
    ui->appTitle->setFocusPolicy(Qt::NoFocus);
    this->resize(800, 500);

    if (touch) {
        this->setWindowState(Qt::WindowMaximized);

        if (not sm->value("CoreApps", "TabletMode")){
            ui->sideView->setVisible(0);
        }
    }

    // all toolbuttons icon size in sideView
    for (QToolButton *b : ui->sideView->findChildren<QToolButton *>()) {
        if (b) {
            b->setIconSize(sideViewIconSize);
        }
    }

    // all toolbuttons icon size in toolBar
    for (QToolButton *b : ui->toolBar->findChildren<QToolButton *>()) {
        if (b) {
            b->setIconSize(toolBarIconSize);
        }
    }

    if (workFilePath.isEmpty()) {
        ui->save->setEnabled(false);
        ui->saveas->setEnabled(false);
        ui->pinIt->setEnabled(false);
        ui->shareIt->setEnabled(false);
        ui->canvasB->setEnabled(false);
        ui->selectionB->setEnabled(false);
        ui->toolsB->setEnabled(false);
        ui->colorB->setEnabled(false);
        ui->miniTools->hide();
    }

    ui->pages->setCurrentIndex(0);

    ui->paintTabs->setCornerWidget(ui->mSizeLabel, Qt::BottomRightCorner);
}

/**
 * @brief Loads application settings
 */
void corepaint::loadSettings()
{
    sideViewSize = sm->value("CoreApps", "SideViewSize");
    sideViewIconSize = sm->value("CoreApps", "SideViewIconSize");
    toolBarIconSize = sm->value("CoreApps", "ToolBarIconSize");
    touch = sm->value("CoreApps", "TouchMode");
    tablet = sm->value("CoreApps", "TabletMode");
    saveLocation = QString(sm->value("CoreShot", "SaveLocation"));
}

void corepaint::shotcuts()
{
    QShortcut *shortcut;
    shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_O), this);
    connect(shortcut, &QShortcut::activated, this, &corepaint::on_open_clicked);
    shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_S), this);
    connect(shortcut, &QShortcut::activated, this, &corepaint::on_save_clicked);
    shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S), this);
    connect(shortcut, &QShortcut::activated, this, &corepaint::on_saveas_clicked);
    shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Z), this);
    connect(shortcut, SIGNAL(activated()), this, SLOT(undo()));
    shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_Y), this);
    connect(shortcut, SIGNAL(activated()), this, SLOT(redo()));
    shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_C), this);
    connect(shortcut, &QShortcut::activated, this, &corepaint::on_copy_clicked);
    shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_X), this);
    connect(shortcut, &QShortcut::activated, this, &corepaint::on_cut_clicked);
    shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_V), this);
    connect(shortcut, &QShortcut::activated, this, &corepaint::on_past_clicked);
    shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_N), this);
    connect(shortcut, &QShortcut::activated, this, &corepaint::on_newtab_clicked);
    shortcut = new QShortcut(QKeySequence(Qt::CTRL + Qt::Key_B), this);
    connect(shortcut, &QShortcut::activated, this, &corepaint::on_pinIt_clicked);
    shortcut = new QShortcut(QKeySequence(Qt::Key_Delete), this);
    connect(shortcut, &QShortcut::activated, this, &corepaint::on_delet_clicked);
}

void corepaint::initializeNewTab(const bool &isOpen, const QString &filePath)
{
    if (ui->paintTabs->count() < 4) {
        ImageArea *imageArea;
        QString fileName;

        if (isOpen && filePath.isEmpty()) {
            imageArea = new ImageArea(isOpen, "", this);
            fileName = imageArea->getFileName();
        } else if (isOpen && !filePath.isEmpty()) {
            imageArea = new ImageArea(isOpen, filePath, this);
            fileName = imageArea->getFileName();
        } else {
            imageArea = new ImageArea(false, "", this);
            fileName = (tr("UntitledImage_") + QString::number(ui->paintTabs->count()));
        }

        if (!imageArea->getFileName().isNull()) {
            QScrollArea *scrollArea = new QScrollArea();
            scrollArea->setAttribute(Qt::WA_DeleteOnClose);

            //scrollArea->setBackgroundRole(QPalette::Shadow);
            scrollArea->setFrameStyle(QFrame::NoFrame);

            scrollArea->setWidget(imageArea);

            ui->paintTabs->addTab(scrollArea, fileName);
            ui->paintTabs->setCurrentIndex(ui->paintTabs->count() - 1);

            mUndoStackGroup->addStack(imageArea->getUndoStack());
            connect(imageArea, SIGNAL(sendPrimaryColorView()), this, SLOT(setPrimaryColorView()));
            connect(imageArea, SIGNAL(sendSecondaryColorView()), this, SLOT(setSecondaryColorView()));
            connect(imageArea, SIGNAL(sendRestorePreviousInstrument()), this,
                    SLOT(restorePreviousInstrument()));
            connect(imageArea, SIGNAL(sendSetInstrument(InstrumentsEnum)), this,
                    SLOT(setInstrument(InstrumentsEnum)));
            connect(imageArea, SIGNAL(sendNewImageSize(QSize)), this,
                    SLOT(setNewSizeToSizeLabel(QSize)));
            connect(imageArea, SIGNAL(sendEnableCopyCutActions(bool)), this,
                    SLOT(enableCopyCutActions(bool)));
            connect(imageArea, SIGNAL(sendEnableSelectionInstrument(bool)), this,
                    SLOT(instumentsAct(bool)));

            //setWindowTitle(QString("%1 - EasyPaint").arg(fileName));
        } else {
            delete imageArea;
        }

        workFilePath = fileName;

        if (ui->paintTabs->count() >= 1) {
            ui->save->setEnabled(true);
            ui->saveas->setEnabled(true);
            ui->pinIt->setEnabled(true);
            ui->shareIt->setEnabled(true);
            ui->canvasB->setEnabled(true);
            ui->selectionB->setEnabled(true);
            ui->toolsB->setEnabled(true);
            ui->colorB->setEnabled(true);
            ui->miniTools->show();
        }

        if (!fileName.isEmpty()) {
            // Function from LibCPrime
			CPrime::InfoFunc::messageEngine(
				"CorePaint",
				"corepaint",
				"File opened",
				"File opened successfully",
				this
			);
        } else {
            // Function from LibCPrime
			CPrime::InfoFunc::messageEngine(
				"CorePaint",
				"corepaint",
				"File not opened",
				"Error in opening the file.",
				this
			);
        }
    } else {
		// Function from LibCPrime
		CPrime::InfoFunc::messageEngine(
			"CorePaint",
			"corepaint",
			"File not opened",
			"You have reached the maximum limit of open tabs",
			this
		);
    }
}

int corepaint::tabsCount()
{
    return ui->paintTabs->count();
}

void corepaint::initializeMainMenu()
{
    QAction *tempAct = mUndoStackGroup->createUndoAction(this);
    tempAct->setIcon(QIcon::fromTheme("edit-undo"));
    ui->undo->setDefaultAction(tempAct);

    tempAct = mUndoStackGroup->createRedoAction(this);
    tempAct->setIcon(QIcon::fromTheme("edit-redo"));
    ui->redo->setDefaultAction(tempAct);

	mInstrumentsMap.clear();
	mInstrumentsMap.insert(CURSOR, ui->select);
	mInstrumentsMap.insert(ERASER, ui->eraser);
	mInstrumentsMap.insert(COLORPICKER, ui->colorpicker);
	mInstrumentsMap.insert(PEN, ui->pen);
	mInstrumentsMap.insert(LINE, ui->line);
	mInstrumentsMap.insert(SPRAY, ui->spray);
	mInstrumentsMap.insert(FILL, ui->fill);
	mInstrumentsMap.insert(RECTANGLE, ui->rectangle);
	mInstrumentsMap.insert(ELLIPSE, ui->ellipse);
	mInstrumentsMap.insert(CURVELINE, ui->curve);
	mInstrumentsMap.insert(TEXT, ui->text);

	Q_FOREACH(QToolButton *btn, mInstrumentsMap) {
		connect(btn, &QToolButton::toggled, this, &corepaint::instumentsAct);
	}

    mPColorChooser = new ColorChooser(0, 0, 0, this);
    mPColorChooser->setStatusTip(tr("Primary color"));
    mPColorChooser->setToolTip(tr("Primary color"));
    connect(mPColorChooser, SIGNAL(sendColor(QColor)), this,
            SLOT(primaryColorChanged(QColor)));

    mSColorChooser = new ColorChooser(255, 255, 255, this);
    mSColorChooser->setStatusTip(tr("Secondary color"));
    mSColorChooser->setToolTip(tr("Secondary color"));
    connect(mSColorChooser, SIGNAL(sendColor(QColor)), this, SLOT(secondaryColorChanged(QColor)));

    ui->cc->addWidget(mPColorChooser);
    ui->cc->addWidget(mSColorChooser);

    connect(ui->toolSize, SIGNAL(valueChanged(int)), this, SLOT(penValueChanged(int)));
}

void corepaint::penValueChanged(const int &value)
{
    DataSingleton::Instance()->setPenSize(value);
}

void corepaint::primaryColorChanged(const QColor &color)
{
    DataSingleton::Instance()->setPrimaryColor(color);
}

void corepaint::secondaryColorChanged(const QColor &color)
{
    DataSingleton::Instance()->setSecondaryColor(color);
}

void corepaint::setPrimaryColorView()
{
    mPColorChooser->setColor(DataSingleton::Instance()->getPrimaryColor());
}

void corepaint::setSecondaryColorView()
{
    mSColorChooser->setColor(DataSingleton::Instance()->getSecondaryColor());
}

ImageArea *corepaint::getCurrentImageArea()
{
    if (ui->paintTabs->currentWidget()) {
        QScrollArea *tempScrollArea = qobject_cast<QScrollArea *>(ui->paintTabs->currentWidget());
        ImageArea *tempArea = qobject_cast<ImageArea *>(tempScrollArea->widget());
        return tempArea;
    }

    return nullptr;
}

ImageArea *corepaint::getImageAreaByIndex(int index)
{
    QScrollArea *sa = static_cast<QScrollArea *>(ui->paintTabs->widget(index));
    ImageArea *ia = static_cast<ImageArea *>(sa->widget());
    return ia;
}

void corepaint::setNewSizeToSizeLabel(const QSize &size)
{
    ui->mSizeLabel->setText(QString("%1 x %2").arg(size.width()).arg(size.height()));
}

void corepaint::closeEvent(QCloseEvent *event)
{
    if (!isSomethingModified() || closeAllTabs()) {
        event->ignore();
        // Function from LibCPrime
        CPrime::InfoFunc::saveToRecent("corepaint", workFilePath);
        event->accept();
    } else {
        event->ignore();
    }
}

bool corepaint::isSomethingModified()
{
    for (int i = 0; i < ui->paintTabs->count(); ++i) {
        if (getImageAreaByIndex(i)->getEdited()) {
            return true;
        }
    }

    return false;
}

bool corepaint::closeAllTabs()
{
    while (ui->paintTabs->count() != 0) {
        ImageArea *ia = getImageAreaByIndex(0);

        if (ia->getEdited()) {
            QString msg = QString("This file contains unsaved changes.\nHow would you like to proceed?");
            QMessageBox message(QMessageBox::Question, QString("Save Changes"), msg,
                                QMessageBox::Yes | QMessageBox::Default | QMessageBox::No | QMessageBox::Cancel |
                                QMessageBox::Escape, this);
            message.setWindowIcon(QIcon(":/icons/corepaint.svg"));

            int reply = message.exec();

            switch (reply) {
                case QMessageBox::Yes:
                    if (ia->save()) {
                        break;
                    }

                    return false;

                case QMessageBox::Cancel:
                    return false;
            }
        }

        QWidget *wid = ui->paintTabs->widget(0);
        ui->paintTabs->removeTab(0);
        delete wid;
    }

    return true;
}

void corepaint::setAllInstrumentsUnchecked(/*QAction *action*/QToolButton *actBtn)
{
    clearImageSelection();

//    foreach (QAction *temp, mInstrumentsActMap) {
//		if (temp != action) {
//			temp->setChecked(false);
//		}
//	}

	Q_FOREACH(QToolButton *btn, mInstrumentsMap) {
		if (btn != actBtn) {
			btn->setChecked(false);
        }
    }
}

void corepaint::setInstrumentChecked(InstrumentsEnum instrument)
{
    setAllInstrumentsUnchecked(nullptr);

    if (instrument == NONE_INSTRUMENT || instrument == INSTRUMENTS_COUNT) {
        return;
    }

//    mInstrumentsActMap[instrument]->setChecked(true);
	mInstrumentsMap[instrument]->setChecked(true);
}

void corepaint::instumentsAct(bool state)
{
//    QAction *currentAction = static_cast<QAction *>(sender());
	QToolButton *currbtn = static_cast<QToolButton *>(sender());

    if (state) {
//        if (currentAction == mInstrumentsActMap[COLORPICKER] && !mPrevInstrumentSetted) {
		if (currbtn == mInstrumentsMap[COLORPICKER] && !mPrevInstrumentSetted) {
            DataSingleton::Instance()->setPreviousInstrument(DataSingleton::Instance()->getInstrument());
            mPrevInstrumentSetted = true;
        }

//        setAllInstrumentsUnchecked(currentAction);
//        currentAction->setChecked(true);
//		  DataSingleton::Instance()->setInstrument(mInstrumentsActMap.key(currentAction));
//        emit sendInstrumentChecked(mInstrumentsActMap.key(currentAction));
		setAllInstrumentsUnchecked(currbtn);
		currbtn->setChecked(true);
		DataSingleton::Instance()->setInstrument(mInstrumentsMap.key(currbtn));
		emit sendInstrumentChecked(mInstrumentsMap.key(currbtn));
    } else {
        setAllInstrumentsUnchecked(nullptr);
        DataSingleton::Instance()->setInstrument(NONE_INSTRUMENT);
        emit sendInstrumentChecked(NONE_INSTRUMENT);

//        if (currentAction == mInstrumentsActMap[CURSOR]) {
//            DataSingleton::Instance()->setPreviousInstrument(mInstrumentsActMap.key(currentAction));
//        }
		if (currbtn == mInstrumentsMap[CURSOR]) {
			DataSingleton::Instance()->setPreviousInstrument(mInstrumentsMap.key(currbtn));
		}
    }
}

void corepaint::enableCopyCutActions(bool enable)
{
    ui->copy->setEnabled(enable);
    ui->cut->setEnabled(enable);
}

void corepaint::clearImageSelection()
{
    if (getCurrentImageArea()) {
        getCurrentImageArea()->clearSelection();
        DataSingleton::Instance()->setPreviousInstrument(NONE_INSTRUMENT);
    }
}

void corepaint::restorePreviousInstrument()
{
    setInstrumentChecked(DataSingleton::Instance()->getPreviousInstrument());
    DataSingleton::Instance()->setInstrument(DataSingleton::Instance()->getPreviousInstrument());
    emit sendInstrumentChecked(DataSingleton::Instance()->getPreviousInstrument());
    mPrevInstrumentSetted = false;
}

void corepaint::setInstrument(InstrumentsEnum instrument)
{
    setInstrumentChecked(instrument);
    DataSingleton::Instance()->setInstrument(instrument);
    emit sendInstrumentChecked(instrument);
    mPrevInstrumentSetted = false;
}

void corepaint::on_paintTabs_currentChanged(int index)
{
    //activateTab
    if (index == -1) {
        return;
    }

    ui->paintTabs->setCurrentIndex(index);
    getCurrentImageArea()->clearSelection();

    QSize size = getCurrentImageArea()->getImage()->size();
    ui->mSizeLabel->setText(QString("%1 x %2").arg(size.width()).arg(size.height()));

    if (!getCurrentImageArea()->getFileName().isEmpty()) {
        workFilePath = QDir(workFilePath).path() + "/" + getCurrentImageArea()->getFileName();
    } else {
        workFilePath = "Untitled Image";
    }

    mUndoStackGroup->setActiveStack(getCurrentImageArea()->getUndoStack());

    //enableActions
    //if index == -1 it means, that there is no tabs
    bool isEnable = index == -1 ? false : true;

    if (!isEnable) {
        setAllInstrumentsUnchecked(nullptr);
        DataSingleton::Instance()->setInstrument(NONE_INSTRUMENT);
        emit sendInstrumentChecked(NONE_INSTRUMENT);
    }

    this->setWindowTitle(ui->paintTabs->tabText(index) + " - CorePaint");
}

void corepaint::on_paintTabs_tabCloseRequested(int index)
{
    ImageArea *ia = getImageAreaByIndex(index);

    if (ia->getEdited()) {
        QString msg = QString("This file contains unsaved changes.\nHow would you like to proceed?");
        QMessageBox message(QMessageBox::Question, QString("Save Changes"), msg,
                            QMessageBox::Yes | QMessageBox::Default |
                            QMessageBox::No | QMessageBox::Cancel | QMessageBox::Escape, this);
        message.setWindowIcon(QIcon(":/icons/corepaint.svg"));

        int reply = message.exec();

        switch (reply) {
            case QMessageBox::Yes:
                if (ia->save()) {
                    break;
                }

                return;

            case QMessageBox::Cancel:
                return;
        }
    }

    mUndoStackGroup->removeStack(ia->getUndoStack());   //for safety
    QWidget *wid = ui->paintTabs->widget(index);
    ui->paintTabs->removeTab(index);
    delete wid;

    if (ui->paintTabs->count() == 0) {
        ui->save->setEnabled(false);
        ui->saveas->setEnabled(false);
        ui->pinIt->setEnabled(false);
        ui->shareIt->setEnabled(false);
        ui->canvasB->setEnabled(false);
        ui->selectionB->setEnabled(false);
        ui->toolsB->setEnabled(false);
        ui->colorB->setEnabled(false);
        ui->miniTools->hide();
        on_menuB_clicked();
    }
}

void corepaint::on_newtab_clicked()
{
    initializeNewTab();
}

void corepaint::on_open_clicked()
{
    initializeNewTab(true);
}

void corepaint::on_save_clicked()
{
    if (getCurrentImageArea()->save()) {
        ui->paintTabs->setTabText(ui->paintTabs->currentIndex(),
                                  getCurrentImageArea()->getFileName().isEmpty() ?
                                  tr("Untitled Image") : getCurrentImageArea()->getFileName());

        workFilePath = getCurrentImageArea()->mFilePath;
		// Function from LibCPrime
		CPrime::InfoFunc::messageEngine(
			"CorePaint",
			"corepaint",
			"File Save",
			"File saved successfully",
			this
		);
    } else {
		// Function from LibCPrime
		CPrime::InfoFunc::messageEngine(
			"CorePaint",
			"corepaint",
			"File not saved",
			"CorePaint was unable to save the file.",
			this
		);
    }
}

void corepaint::on_saveas_clicked()
{
    if (getCurrentImageArea()->saveAs()) {
        ui->paintTabs->setTabText(ui->paintTabs->currentIndex(),
                                  getCurrentImageArea()->getFileName().isEmpty() ?
                                  tr("Untitled Image") : getCurrentImageArea()->getFileName());

        workFilePath = getCurrentImageArea()->mFilePath;
		// Function from LibCPrime
		CPrime::InfoFunc::messageEngine(
			"CorePaint",
			"corepaint",
			"File Save",
			"File saved successfully",
			this
		);
    } else {
		// Function from LibCPrime
		CPrime::InfoFunc::messageEngine(
			"CorePaint",
			"corepaint",
			"File not saved",
			"CorePaint was unable to save the file.",
			this
		);
    }
}

void corepaint::on_resizeimage_clicked()
{
    getCurrentImageArea()->resizeImage();
}

void corepaint::on_resizecanvas_clicked()
{
    getCurrentImageArea()->resizeCanvas();
}

void corepaint::on_rotateright_clicked()
{
    getCurrentImageArea()->rotateImage(true);
}

void corepaint::on_rotateleft_clicked()
{
    getCurrentImageArea()->rotateImage(false);
}

//void corepaint::on_zoomin_clicked()
//{
//    getCurrentImageArea()->zoomImage(2.0);
//    getCurrentImageArea()->setZoomFactor(2.0);
//}

//void corepaint::on_zoomout_clicked()
//{
//    getCurrentImageArea()->zoomImage(0.5);
//    getCurrentImageArea()->setZoomFactor(0.5);
//}

void corepaint::on_cut_clicked()
{
    if (ImageArea *imageArea = getCurrentImageArea()) {
        imageArea->cutImage();
    }
}

void corepaint::on_copy_clicked()
{
    if (ImageArea *imageArea = getCurrentImageArea()) {
        imageArea->copyImage();
    }
}

void corepaint::on_past_clicked()
{
    if (ImageArea *imageArea = getCurrentImageArea()) {
        imageArea->pasteImage();
    }
}

void corepaint::undo()
{
    mUndoStackGroup->undo();
}

void corepaint::redo()
{
    mUndoStackGroup->redo();
}

void corepaint::on_delet_clicked()
{
    if (ImageArea *imageArea = getCurrentImageArea()) {
        imageArea->clearBackground();
    }
}

void corepaint::pageClick(QToolButton *btn, int i)
{
    // all button checked false
    for (QToolButton *b : ui->toolBar->findChildren<QToolButton *>()) {
        b->setChecked(false);
    }
    if (touch) {
        if (!tablet){
            if(ui->sideView->isVisible()){
                if(ui->pages->currentIndex() == i)
                    ui->sideView->setVisible(0);
            } else{

                ui->sideView->setVisible(1);
            }
        }
    }

    btn->setChecked(true);
    ui->pages->setCurrentIndex(i);
}

void corepaint::on_menuB_clicked()
{
    pageClick(ui->menuB, 0);
}

void corepaint::on_canvasB_clicked()
{
    pageClick(ui->canvasB, 1);
}

void corepaint::on_selectionB_clicked()
{
    pageClick(ui->selectionB, 2);
}

void corepaint::on_toolsB_clicked()
{
    pageClick(ui->toolsB, 3);
}

void corepaint::on_colorB_clicked()
{
    pageClick(ui->colorB, 4);
}

void corepaint::sendFiles(const QStringList &paths)
{
    if (paths.count()) {
        foreach (QString str, paths) {
            if (CPrime::FileUtils::isFile( str ) ) {
                initializeNewTab( true, str );
            } else {
                qDebug() << "func(sendFiles) : Error : Invalid filepath got :" << str;
            }
        }
    }
}

void corepaint::on_pinIt_clicked()
{
    if (!QFile(workFilePath).exists()) {
        // Function from LibCPrime
        QString mess = "File: " + workFilePath + "'\ndoes not exist.";
		// Function from LibCPrime
		CPrime::InfoFunc::messageEngine(
			"CorePaint",
			"corepaint",
			"Error",
			mess,
			this
		);
    } else {
        pinit pit;
        pit.callPinIt(this, workFilePath);
    }
}


void corepaint::on_shareIt_clicked()
{
    if (!QFile(workFilePath).exists()) {
        // Function from LibCPrime
        QString mess = "File: " + workFilePath + "'\ndoes not exist.";
		// Function from LibCPrime
		CPrime::InfoFunc::messageEngine(
			"CorePaint",
			"corepaint",
			"Error",
			mess,
			this
		);
    } else {
        shareit::shareFiles( this, QStringList() << workFilePath );
    }
}
