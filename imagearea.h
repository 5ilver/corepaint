/*
    *
    * This file is a part of CorePaint.
    * A paint app for CuboCore Application Suite
    * Copyright 2019 CuboCore Group
    *

    *
    * This program is free software; you can redistribute it and/or modify
    * it under the terms of the GNU General Public License as published by
    * the Free Software Foundation; either version 3 of the License, or
    * (at your option) any later version.
    *

    *
    * This program is distributed in the hope that it will be useful,
    * but WITHOUT ANY WARRANTY; without even the implied warranty of
    * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    * GNU General Public License for more details.
    *

    *
    * You should have received a copy of the GNU General Public License
    * along with this program; if not, write to the Free Software
    * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    * MA 02110-1301, USA.
    *
*/


#ifndef IMAGEAREA_H
#define IMAGEAREA_H


#include <QWidget>
#include <QDir>

#include <cprime/settingsmanage.h>

#include "easypaintenums.h"
#include "additionaltools.h"


QT_BEGIN_NAMESPACE
class QUndoStack;
QT_END_NAMESPACE

class UndoCommand;
class AbstractInstrument;
class AbstractEffect;


class ImageArea : public QWidget {
    Q_OBJECT

public:
    explicit ImageArea(const bool &isOpen, const QString &filePath, QWidget *parent);
    ~ImageArea();

    bool save();
    bool saveAs();
    void resizeImage();
    void resizeCanvas();
    void rotateImage(bool flag);

    inline QString getFileName() {
        return (mFilePath.isEmpty() ? mFilePath :
                mFilePath.split('/').last());
    }
    inline QImage *getImage() {
        return mImage;
    }
    inline QImage getImageCopy() {
        return mImageCopy;
    }
    inline void setImage(const QImage &image) {
        *mImage = image;
    }
    inline void setEdited(bool flag) {
        mIsEdited = flag;
    }
    inline bool getEdited() {
        return mIsEdited;
    }

    void restoreCursor();
    bool zoomImage(qreal factor);
    inline void setZoomFactor(qreal factor) {
        mZoomFactor *= factor;
    }
    inline qreal getZoomFactor() {
        return mZoomFactor;
    }
    inline QUndoStack *getUndoStack() {
        return mUndoStack;
    }
    inline void setIsPaint(bool isPaint) {
        mIsPaint = isPaint;
    }
    inline bool isPaint() {
        return mIsPaint;
    }
    inline void emitPrimaryColorView() {
        emit sendPrimaryColorView();
    }
    inline void emitSecondaryColorView() {
        emit sendSecondaryColorView();
    }
    inline void emitColor(QColor &color) {
        emit sendColor(color);
    }
    inline void emitRestorePreviousInstrument() {
        emit sendRestorePreviousInstrument();
    }

    void copyImage();
    void pasteImage();
    void cutImage();
    void clearBackground();
    void saveImageChanges();
    void clearSelection();
    void pushUndoCommand(UndoCommand *command);

    QString mFilePath; /** Path where located image. */

private:
    QImage *mImage,  /**< Main image. */
           mImageCopy; /**< Copy of main image, need for events. */ // ?????????????
    AdditionalTools *mAdditionalTools;
    QString mOpenFilter; /**< Supported open formats filter. */
    QString mSaveFilter; /**< Supported save formats filter. */
    bool mIsEdited, mIsPaint, mIsResize, mRightButtonPressed;
    QPixmap *mPixmap;
    QCursor *mCurrentCursor;
    qreal mZoomFactor;
    QUndoStack *mUndoStack;
    QVector<AbstractInstrument *> mInstrumentsHandlers;
    AbstractInstrument *mInstrumentHandler;

    QAction *copyAct;
    QAction *cutAct;
    QAction *pasteAct;
    QAction *delAct;

    void initializeImage();
    void open();
    void open(const QString &filePath);
    void drawCursor();
    void makeFormatsFilters();

signals:
    void sendPrimaryColorView();
    void sendSecondaryColorView();
    void sendNewImageSize(const QSize &);
    void sendColor(const QColor &);
    void sendRestorePreviousInstrument();
    void sendSetInstrument(InstrumentsEnum);
    void sendEnableCopyCutActions(bool enable);
    void sendEnableSelectionInstrument(bool enable);

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void paintEvent(QPaintEvent *event);

};

#endif // IMAGEAREA_H
